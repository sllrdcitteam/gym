<!DOCTYPE html>
<html>
<head>
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</head>
<body>

	<img src="">

<h2><center>Membership Registration for the Kimbulawela Gymnasium</center></h2>


<div class="container">
	<h3>Personal Information</h3>
	<hr>

	<form action="http://localhost/gymnasium/gymcon.php" method="POST">

	<div class="row">
			<div class="col">Full Name
		      <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Fullname" aria-label="Username" aria-describedby="addon-wrapping">
			</div>
		
		<div class="col">Gender :<br>
		  <div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="gender" id="gender" value="Male">
			  <label class="form-check-label" for="inlineRadio1">Male</label>
		  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="gender" id="gender" value="Female">
				  <label class="form-check-label" for="inlineRadio2">Female</label></div>
		</div> 
	</div>


	<div class="row">
		<div class="col"> 
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
		    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
		  </div>
		</div>

		<div class="col">
		Permanent Address 
		  <input type="text" class="form-control" id="address" name="address" placeholder="Permanent Address" aria-label="Username" aria-describedby="addon-wrapping">
		</div>


	</div>

	<div class="row">
		<div class="col">
			Date of Birth: <input type="date" class="form-control" name="bday" id="bday">
		</div>

		<div class="col">
			Occupation :
			<input type="text" class="form-control" id="occup" name="occup" placeholder="Software Engineer">
		</div>

	</div>

	<div class="row">
		<div class="col">
			<div class="form-group">
			    <label for="exampleFormControlSelect1">NIC / Passport No / Driving Licence No</label>
			    <select class="form-control" id="nicpass" name="nicpass">
			      <option value="Did not Select">-Select-</option>
			      <option value="12345678v">12345678v</option>
			    </select>
  			</div>
		</div>

		<div class="col">
		</div>

	</div>

	<h3>Medical Information</h3>
	<hr>

	<div class="row">

		<div class="col">
			Advice from physician whether to exercise or not<br>
			  <div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="physician" id="physician" value="Yes">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="physician" id="physician" value="No">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
		</div> 
	

		<div class="col">
		</div>

		<div class="col">
			
		</div>

		<div class="col">
		</div>
	</div>
	
	

	&nbsp;IF YOU HAVE SUFFERED ANY OF FOLLOWING CONDITIONS, PLEASE SUBMITS REPORT/LETTER FROM YOUR DOCTOR (INFORMATIONS IS NEEDED PRIOR TO TRAINING)

	<div class="row">

		<div class="col">

			Heart Attack<br>
			  <div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="heart" id="heart" value="Yes">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="heart" id="heart" value="NO">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
		</div> 
			
		

		<div class="col">
			Diabetes<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="diabetes" id="diabetes" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="diabetes" id="diabetes" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
		</div> 

		

		<div class="col">
			High Blood Pressure<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="bloodp" id="bloodp" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="bloodp" id="bloodp" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
		</div> 
			
		

		<div class="col">
			Hernia<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="hernia" id="hernia" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="hernia" id="hernia" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
		</div> 

		

	</div>

	<div class="row">
		<div class="col">

			Pain or Tightness in the chest<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="pain" id="pain" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="pain" id="pain" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
			
		</div>

		<div class="col">

			Asthma<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="asthma" id="asthma" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="asthma" id="asthma" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>

		</div>

		<div class="col">

			High Cholesterol<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="cholesterol" id="cholesterol" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="cholesterol" id="cholesterol" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
			
		</div>

		<div class="col">

			Fainting Attacks<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="faintinga" id="faintinga" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="faintinga" id="faintinga" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>

		</div>

	</div>

	<div class="row">
		<div class="col">

			Stomach or Duodenal Ulcer<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="stomach" id="stomach" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="stomach" id="stomach" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
			
		</div>

		<div class="col">

			Back Problem<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="backp" id="backp" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="backp" id="backp" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>

		</div>

		<div class="col">

			Liver of Kidney Condition<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="liverofkidney" id="liverofkidney" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="liverofkidney" id="liverofkidney" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
			
		</div>

		<div class="col">

			Epilepsy or Fits<br>
			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="epilepsy" id="epilepsy" value="option1">
				  <label class="form-check-label" for="inlineRadio1">Yes</label>
			  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="epilepsy" id="epilepsy" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>

		</div>

	</div>

	<div class="row">
			<div class="col">
				Difficulty in breathing or Chronic cough <br>
		      <div class="form-check form-check-inline">
			  <input class="form-check-input" type="radio" name="breathing" id="breathing" value="option1">
			  <label class="form-check-label" for="inlineRadio1">Yes</label>
		  </div>

			<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="breathing" id="breathing" value="option2">
				  <label class="form-check-label" for="inlineRadio2">No</label></div>
			</div>
		
		<div class="col">

			Additional medical notes
		      <input type="text" class="form-control" id="medicalnote" name="medicalnote" placeholder="Type your info" aria-label="Username" aria-describedby="addon-wrapping">
		  
		</div> 
	</div>

	<h3>Contact Information</h3>
	<hr>

	<div class="row">
		<div class="col">
			Telephone - Mobile: <input type="text" class="form-control" id="telem" name="telem" placeholder="eg: 0711234567" aria-label="Username" aria-describedby="addon-wrapping">
		</div>

		<div class="col">
			Telephone - Home :
			<input type="text" class="form-control" id="teleh" name="teleh" placeholder="eg: 0112912345" aria-label="Username" aria-describedby="addon-wrapping">
		</div>

	</div>

	<div class="row">
	    <div class="col">Emergency Contact Person
	      <input type="text" class="form-control" id="emergencyc" name="emergencyc"  placeholder="eg: Kamal Pathirana">
	    </div>

	    <div class="col">Emergency Contact Person Contact No
	      <input type="text" class="form-control" id="emergencycno" name="emergencycno"  placeholder="Last name">
	    </div>
  	</div>

  	<div class="row">
		
		  Emergency Contact Person Address
		  <textarea class="form-control" id="comment" name="comment" aria-label="With textarea"></textarea>
		
	</div>

	<h3>Other Information</h3>
	<hr>

	<div class="row">
	    <div class="col">Membership Type
	        <select class="form-control">
			  <option>Default select</option>
		    </select>
	    </div>

	    <div class="col">Membership (Duration)
	      	<select class="form-control">
			  <option>Default select</option>
		    </select>
	    </div>

	    <div class="col">Membership (Price)
	      <input type="text" class="form-control" placeholder="">
	    </div>
  	</div>

  	<div class="row">
  		<div class="col">Reason for join this Gym
  			<select class="form-control">
			  <option>Default select</option>
		    </select>
  		</div>
  		
  		<div class="col">

  		</div>		

  	</div>

  	<div class="row">Upload your photo
  		<div class="input-group mb-3">
		  <div class="input-group-prepend">
		    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
		  </div>
		  <div class="custom-file">
		    <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
		    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
		  </div>
		</div>

	</div>

	<div class="row">
		<div class="form-check">
		  <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
		  <label class="form-check-label" for="defaultCheck1">
		    I hereby certify that the above information declared is true and correct to the best of my knowledge
		  </label>
		</div>
	</div>

		<button type="button" class="btn btn-primary">Back to Home</button>
		<button type="reset" class="btn btn-danger">Reset</button>
		<button type="submit" class="btn btn-success" id="submit" name="submit">Submit</button>
	</form>
	  
</div>
	  
	  


</body>
</html>